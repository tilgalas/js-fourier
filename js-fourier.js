function setCanvasTransform(canvas) {
    var ctx = canvas.getContext("2d");
    ctx.setTransform(1, 0, 0, -1, canvas.width / 2, canvas.height / 2);
}

function clearCanvasTransform(canvas) {
    var ctx = canvas.getContext("2d");
    ctx.setTransform(1, 0, 0, 1, 0, 0);
}

function createCanvasContext(canvasId) {
    var canvas = document.getElementById(canvasId);
    setCanvasTransform(canvas);
    return canvas.getContext("2d");
}

function applyTransformMatrix(transform, x, y) {
    var v = [x, y, 1];
    var vt = math.multiply(transform, v)
    var vtnorm = math.divide(vt, vt.subset(math.index(2)));
    return vtnorm.subset(math.index([0, 1])).toArray();
}

function drawComplex(ctx, c, transform = math.identity(3, 3)) {
    var ct = applyTransformMatrix(transform, c.re, c.im);
    var orig = applyTransformMatrix(transform, 0, 0);

    ctx.beginPath();
    ctx.moveTo(orig[0], orig[1]);
    ctx.lineTo(ct[0], ct[1]);
    ctx.stroke();
}

function drawChart(ctx, ys, transform = math.identity(3, 3)) {
    ctx.beginPath();
    var orig = applyTransformMatrix(transform, 0, ys[0]);
    ctx.moveTo(orig[0], orig[1]);
    $.each(ys, (x, y) => {
        var v = applyTransformMatrix(transform, x, y);
        ctx.lineTo(v[0], v[1]);
    });
    ctx.stroke();

}

function drawGrid(ctx, range, ticks, tickH = 4, transform = math.identity(3, 3)) {
    var orig = applyTransformMatrix(transform, 0, 0);
    var rangeTransformed = applyTransformMatrix(transform, range, 0)[0];
    ctx.save();
    ctx.setLineDash([5, 5]);
    ctx.beginPath();
    ctx.moveTo(orig[0], orig[1]);

    ctx.lineTo(rangeTransformed, orig[1]);
    for (var i = 1; i < ticks; i++) {
        var x = i * range / ticks;
        var p0 = applyTransformMatrix(transform, x, -tickH / 2);
        var p1 = applyTransformMatrix(transform, x, tickH / 2);
        ctx.moveTo(p0[0], p0[1]);
        ctx.lineTo(p1[0], p1[1]);
    }
    ctx.stroke();
    ctx.restore();
}

function clearCanvas(ctx) {
    ctx.clearRect(
        -ctx.canvas.width / 2,
        -ctx.canvas.height / 2,
        ctx.canvas.width,
        ctx.canvas.height);
}


function baseC(i, f, n) {
    return math.exp(math.multiply(math.complex(0, 1), 2 * math.pi * i * f / n));
}

function Dft(signal, f) {
    this.signal = signal;
    this.n = signal.length;
    this.f = f;
    this.sums = signal.reduce((acc, x, j) => {
        acc.curSum = math.add(acc.curSum, math.multiply(
            x, baseC(j, f, this.n)
        ));
        acc.intermediates.push(acc.curSum);
        return acc;
    }, {curSum: math.complex(0, 0), intermediates: []});
    this.F = this.sums.curSum;
    this.sums = this.sums.intermediates;
    this.inv = math.range(0, this.n).map(k => {
        return math.multiply(this.F, baseC(-k, f, this.n), 1 / this.n);
    });
}

function drawDot(ctx, x, y, transform = math.identity(3, 3)) {
    c = applyTransformMatrix(transform, x, y);
    ctx.beginPath();
    ctx.arc(c[0], c[1], 2, 0, 2 * math.pi);
    ctx.fill();
}

function FourierAnimation(
    dfts,
    f,
    frameDuration,
    canvas,
    backgroundCanvas,
    transform = math.identity(3, 3)) {
    this.gridStyle = "black";
    this.signalStyle = "red";
    this.FStyle = "blue";

    this.progress = 0.0;
    this.frameDuration = frameDuration;
    this.lastDraw = null;
    this.dfts = dfts;
    this.F = dfts.map(dft => dft.F.abs());
    this.transform = transform;
    this.status = "stopped";
    this.canvas = canvas;
    this.backgroundCanvas = backgroundCanvas;
    this.f = f;
    this.n = dfts[f].signal.length;
    this.xf = (backgroundCanvas.width * (5 / 6)) / this.n;
    this.sumScaleF = 0.01;
    this.chartTransform = math.matrix(
            [[this.xf, 0, -backgroundCanvas.width * (5 / 12)],
             [0, 40, backgroundCanvas.height / 3],
             [0, 0, 1]]);
    this.FTransform = math.matrix(
            [[this.xf, 0, -backgroundCanvas.width * (5 / 12)],
             [0, 100 * this.sumScaleF, -backgroundCanvas.height / 3],
             [0, 0, 1]]);

    this.initBackground = function() {
        var ctx = this.backgroundCanvas.getContext('2d');

        clearCanvas(ctx);

        var gridStyle = this.gridStyle;
        var signalStyle = this.signalStyle;
        var FStyle = this.FStyle;
        var chartTransform = this.chartTransform;
        var dft = this.dfts[this.f];
        var FTransform = this.FTransform;

        ctx.strokeStyle = gridStyle;
        drawGrid(ctx, dft.signal.length, f, 4, chartTransform);

        // let's draw only half of the sinusoids, since the fourier
        // transform is symmetrical around the n / 2 axis, but in turn
        // increase their amplitude twofold to compensate
        ctx.strokeStyle = "rgba(0, 0, 255, .2)";
        this.dfts.filter((dft, i) => {
            return i <= this.n / 2 && i !== f && dft.F.abs() > 1e-1;
        }).forEach(dft => {
            drawChart(
                ctx,
                dft.inv.map(c => 2 * c.re).toArray(),
                chartTransform
            );
        });
        ctx.strokeStyle = "rgba(0, 0, 255, 1)";
        drawChart(ctx,
                  dft.inv.map(c => 2 * c.re).toArray(),
                  chartTransform);
        ctx.strokeStyle = signalStyle;
        drawChart(ctx,
                  dft.signal,
                  chartTransform);

        ctx.strokeStyle = gridStyle;
        drawGrid(ctx, this.F.length, this.F.length / 10, 15, FTransform);
        ctx.strokeStyle = FStyle;
        drawChart(ctx, this.F, FTransform);
        ctx.fillStyle = "black";
        drawDot(ctx, f, this.F[f], FTransform);

    };

    this.initBackground();

    this.updateAnimTime = function(timestamp) {
        if (this.lastDraw !== null) {
            this.progress +=
                ((timestamp - this.lastDraw) / (this.dfts.length * this.frameDuration));
            this.progress -= math.floor(this.progress);
        }

        this.lastDraw = timestamp;
    }

    this.calcFrame = function() {
        return this.progress * this.dfts.length;
    }

    this.drawFrame = function(frame) {
        var ctx = this.canvas.getContext("2d");
        var gridStyle = this.gridStyle;
        var signalStyle = this.signalStyle;
        var FStyle = this.FStyle;
        var f = this.f;
        var dft = this.dfts[f];
        var chartTransform = this.chartTransform;
        var FTransform = this.FTransform;
        var frameNo = math.floor(frame) % dft.n;
        var frameNoNext = (frameNo + 1) % dft.n;
        frame = frameNo + math.pow(frame - frameNo, math.log10(this.frameDuration));

        clearCanvasTransform(ctx.canvas);
        ctx.drawImage(this.backgroundCanvas, 0, 0);
        setCanvasTransform(ctx.canvas);

        ctx.fillStyle = "black";
        var signalInterp = linearInterp(dft.signal[frameNo], dft.signal[frameNoNext], frameNo, frameNo + 1, frame);
        drawDot(ctx, frame, signalInterp, chartTransform);

        var base = baseC(frameNo, dft.f, dft.n);
        var baseInterp = baseC(frame, dft.f, dft.n);
        var nextBase = baseC(frameNoNext % dft.n, dft.f, dft.n);
        ctx.strokeStyle = signalStyle;
        drawComplex(ctx, math.complex(signalInterp, 0), this.transform);
        ctx.strokeStyle = "green";
        drawComplex(ctx, baseInterp, this.transform);

        ctx.strokeStyle = FStyle;
        var sumScaleF = this.sumScaleF;
        var sumTransform = math.multiply(
            this.transform, math.matrix([[sumScaleF, 0, 0], [0, sumScaleF, 0], [0, 0, 1]]));
        var sum = linearInterp(dft.sums[frameNo], dft.sums[frameNoNext], frameNo, frameNo + 1, frame);
        drawComplex(ctx, sum, sumTransform);
        this.drawTrace(ctx, f, frameNo, this.dfts.length * 1, 0, 0, 255, sumTransform);
        ctx.strokeStyle = 'magenta';
        drawComplex(ctx,
                    math.multiply(
                        dft.signal[frameNoNext],
                        nextBase),
                    math.multiply(
                        this.transform,
                        math.matrix([
                            [1, 0, dft.sums[frameNo].re * sumScaleF],
                            [0, 1, dft.sums[frameNo].im * sumScaleF],
                            [0, 0, 1]
                        ])
                    ));
    }

    this.drawTrace = function(
        ctx,
        f,
        frameNo,
        traceSize, r, g, b,
        transform = math.identity(3, 3)) {
        for (var i = 0; i < traceSize; i++) {
            var alpha = (i + 0.5) / traceSize / 2;
            var im = (frameNo - traceSize + i) % this.dfts.length;
            if (im < 0) im += this.dfts.length;
            var c = this.dfts[f].sums[im];
            ctx.fillStyle = `rgba(${r}, ${g}, ${b}, ${alpha})`;
            drawDot(ctx, c.re, c.im, transform);
        }
    }

    this.animationCallback = function() {
        var animation = this;
        var inner = function(timestamp) {
            var ctx = animation.canvas.getContext("2d");
            if (animation.status === "playing") {
                animation.updateAnimTime(timestamp);
                var frame = animation.calcFrame();
                clearCanvas(ctx);
                animation.drawFrame(frame);
                window.requestAnimationFrame(inner);
            }
        }
        return inner;
    }

    this.play = function() {
        if (this.status !== "playing") {
            this.status = "playing";
            window.requestAnimationFrame(this.animationCallback());
        }
    }

    this.stop = function() {
        this.status = "stopped";
        this.lastDraw = null;
    }
}

function linearInterp(y1, y2, x1, x2, xm) {
    return interp(
        y1, y2,
        math.divide(
            math.subtract(xm, x1),
            math.subtract(x2, x1)
        ));
}


function interp(y1, y2, t) {
    return math.add(
        y1,
        math.multiply(
            math.subtract(y2, y1),
            t
        )
    );
}

function jqready() {
    var ctx = createCanvasContext("c");
    $("form#anim-settings").data("ctx", ctx);
    var backgroundCanvas = document.createElement("canvas");
    backgroundCanvas.width = ctx.canvas.width;
    backgroundCanvas.height = ctx.canvas.height;
    setCanvasTransform(backgroundCanvas);
    $("form#anim-settings").data("backgroundCtx", backgroundCanvas.getContext("2d"));

    $("form#anim-settings").submit(function(event) {
        event.preventDefault();
        var ctx = $(this).data("ctx");
        var backgroundCtx = $(this).data("backgroundCtx");
        var sigFnBody = $(this).find("#signal").val();
        var sigFn = new Function("x", "x_n", "n", sigFnBody);
        var n = 256;
        var sig = math.range(0, n).map(x => sigFn(x, 2 * math.pi * x / n, n)).toArray();
        var f = parseInt($(this).find("#f").val());
        var dfts = math.range(0, n).map(f => new Dft(sig, f)).toArray();
        var frameDuration = parseInt($(this).find("#frame-duration").val());
        var anim = new FourierAnimation(
            dfts,
            f,
            frameDuration,
            ctx.canvas,
            backgroundCtx.canvas,
            math.matrix([[100, 0, 0], [0, 100, 0], [0, 0, 1]])
        );

        var curAnim = $(this).data("curAnim");
        if (curAnim !== undefined) {
            curAnim.stop();
        }

        $(this).data("curAnim", anim);
        anim.play();

    });

    $("form#anim-settings").find("#play").click(function(event) {
        var form = $(this).parents("form");
        var frameDuration = parseInt(form.find("#frame-duration").val());
        var curAnim = form.data("curAnim");
        if (curAnim !== undefined) {
            curAnim.frameDuration = frameDuration;
            curAnim.play();
        }
    });

    $("form#anim-settings").find("#stop").click(function(event) {
        var form = $(this).parents("form");
        var curAnim = form.data("curAnim");
        if (curAnim !== undefined) {
            curAnim.stop();
        }
    });
}
